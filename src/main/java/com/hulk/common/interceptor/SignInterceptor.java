package com.hulk.common.interceptor;

import com.hulk.common.support.http.HttpCode;
import com.hulk.common.util.FileUtil;
import com.hulk.common.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 签名验证
 * @author hulk
 * @since 2018年5月12日 下午10:40:38
 */
@Slf4j
public class SignInterceptor extends HandlerInterceptorAdapter {

    // 白名单
    private List<String> whiteUrls;
    private int _size = 0;

    public SignInterceptor() {
        // 读取文件
        String path = SignInterceptor.class.getResource("/").getFile();
        whiteUrls = FileUtil.readFile(path + "white/signWhite.txt");
        _size = null == whiteUrls ? 0 : whiteUrls.size();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        // 请求秘钥的接口不需要签名
        String url = request.getRequestURL().toString();
        String refer = request.getHeader("Referer");
        if (refer != null && refer.contains("/swagger") || WebUtil.isWhiteRequest(url, _size, whiteUrls)) {
            log.info("SignInterceptor skip");
            return true;
        }
        String sign = request.getHeader("sign");
        if (sign == null) {
            return WebUtil.write(response, HttpCode.NOT_ACCEPTABLE.value(), "请求参数未签名");
        }
        // 获取参数
        Map<String, Object> params = WebUtil.getParameterMap(request);
        String[] keys = params.keySet().toArray(new String[]{});
        Arrays.sort(keys);
        StringBuilder sb = new StringBuilder();
        for (String key : keys) {
            if (!"dataFile".equals(key)) {
                if (sb.length() > 0) {
                    sb.append("&");
                }
                sb.append(key).append("=").append(params.get(key));
            }
        }
        // 验证信息摘要MD5加密字节转十六进制字符串
        String encrypt = DigestUtils.md5Hex(URLEncoder.encode(sb.toString(), "UTF-8"));
        if (!encrypt.toLowerCase().equals(sign.toLowerCase())) {
            return WebUtil.write(response, HttpCode.UNAUTHORIZED.value(), HttpCode.UNAUTHORIZED.msg());
        }
        log.info("SignInterceptor successful");
        return true;
    }
}
