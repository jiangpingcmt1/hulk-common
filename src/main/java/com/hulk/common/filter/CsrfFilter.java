package com.hulk.common.filter;

import com.hulk.common.util.DateUtil;
import com.hulk.common.util.FileUtil;
import com.hulk.common.util.WebUtil;
import lombok.extern.slf4j.Slf4j;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * 跨站请求拦截
 * @author hulk
 * @since 2018年7月27日 上午10:58:14
 */
@Slf4j
public class CsrfFilter implements Filter {
  

    // 白名单
    private List<String> whiteUrls;

    private int _size = 0;

    @Override
    public void init(FilterConfig filterConfig) {
        log.info("init CsrfFilter..");
        // 读取文件
        String path = CsrfFilter.class.getResource("/").getFile();
        whiteUrls = FileUtil.readFile(path + "white/csrfWhite.txt");
        _size = null == whiteUrls ? 0 : whiteUrls.size();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        try {
            HttpServletRequest req = (HttpServletRequest)request;
            // 获取请求url地址
            String url = req.getRequestURL().toString();
            String referurl = req.getHeader("Referer");
            if (WebUtil.isWhiteRequest(referurl, _size, whiteUrls)) {
                chain.doFilter(request, response);
            } else {
                // 记录跨站请求日志
                log.warn("跨站请求---->>>{} || {} || {} || {}", url, referurl, WebUtil.getHost(req),
                    DateUtil.getDateyyyyMMddHHmmssSSS());
                WebUtil.write(response, 308, "错误的请求头信息");
                return;
            }
        } catch (Exception e) {
            log.error("doFilter", e);
        }
    }

    @Override
    public void destroy() {
        log.info("destroy CsrfFilter.");
    }
}
