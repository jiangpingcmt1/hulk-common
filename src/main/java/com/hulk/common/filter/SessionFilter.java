package com.hulk.common.filter;

import com.hulk.common.support.http.SessionUser;
import com.hulk.common.util.ShiroUtil;
import com.hulk.common.util.WebUtil;
import lombok.extern.slf4j.Slf4j;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * SESSION会话转存REQUEST
 * @author hulk
 * @since 2018年7月22日 上午9:29:55
 */
@Slf4j
public class SessionFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("init SessionFilter.");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        SessionUser sessionUser = ShiroUtil.getCurrentUser();
        if (sessionUser != null) {
            WebUtil.saveCurrentUser((HttpServletRequest)request, sessionUser);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        log.info("destroy SessionFilter.");
    }
}
