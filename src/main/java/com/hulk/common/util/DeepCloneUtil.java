package com.hulk.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author 作者 cmt
 * @version 创建时间：2016年6月1日 下午2:03:12 类说明
 */

public class DeepCloneUtil{


	// 克隆对象必须继承 java.io.Serializable
	@SuppressWarnings("unchecked")
	public <T>T clone(Object o) {
		// 将对象写到流里
		try {
			ByteArrayOutputStream bo = new ByteArrayOutputStream();
			ObjectOutputStream oo = new ObjectOutputStream(bo);
			oo.writeObject(o);
			// 从流里读出来
			ByteArrayInputStream bi = new ByteArrayInputStream(bo.toByteArray());
			ObjectInputStream oi = new ObjectInputStream(bi);
			return   (T)oi.readObject();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		//return null;
	}
	
	public static void main(String[] args) throws Exception {
		String a="5555";
	   String   b =	new DeepCloneUtil().clone("5555");
	   String c=a;
	   if(a.equals(b)){
		   System.out.println(true);
	   }
	   if(!(a==b)){
		   System.out.println(false);
	   }
	   if((a==c)){
		   System.out.println(true);
	   }
	}

}
