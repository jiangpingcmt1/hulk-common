package com.hulk.common.util;


import com.hulk.common.Constants;
import com.hulk.common.support.http.SessionUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.InvalidSessionException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;


@Slf4j
public final class ShiroUtil {


    /** 保存当前用户 */
    public static final void saveCurrentUser(SessionUser user) {
        setSession(Constants.CURRENT_USER, user);
    }

    /**
     * 将一些数据放到ShiroSession中,以便于其它地方使用
     * 比如Controller,使用时直接用HttpSession.getAttribute(key)就可以取到
     */
    public static final void setSession(Object key, SessionUser value) {
        Subject currentUser = SecurityUtils.getSubject();
        if (null != currentUser) {
            Session session = currentUser.getSession();
            if (null != session) {
                session.setAttribute(key, value);
            }
        }
    }

    /** 获取当前用户 */
    public static final SessionUser getCurrentUser() {
        Subject currentUser = SecurityUtils.getSubject();
        if (null != currentUser) {
            try {
                Session session = currentUser.getSession();
                if (null != session) {
                    return (SessionUser)session.getAttribute(Constants.CURRENT_USER);
                }
            } catch (InvalidSessionException e) {
                log.error("", e);
            }
        }
        return null;
    }

}
