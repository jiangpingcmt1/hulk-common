package com.hulk.common.util;

import lombok.extern.slf4j.Slf4j;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程辅助类
 * @author hulk
 * @since 2018年7月27日 下午7:00:11
 */
@Slf4j
public final class ThreadUtil {


    public static void sleep(int start, int end) {
        try {
            Thread.sleep(MathUtil.getRandom(start, end).longValue());
        } catch (InterruptedException e) {
            log.error(ExceptionUtil.getStackTraceAsString(e));
        }
    }

    public static void sleep(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            if (log.isDebugEnabled()) {
                log.debug(e.getMessage());
            }
        }
    }

    public static ExecutorService threadPool(int core, int max, int seconds) {
        return new ThreadPoolExecutor(core, max, seconds, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());
    }
}
