
package com.hulk.common.util;
/**
 * 各进制全局唯一数
* @author cmt  
* @E-mail:29572320@qq.com
* @version Create on:  2017年5月5日 下午4:23:55
* Class description
*/


import cn.ms.sequence.IdWorker;

public class IDWorkerRadix {


	public static String getId(){
		return String.valueOf(IdWorker.getId());
	}

	public static String getBinaryId(){
		return Long.toBinaryString(IdWorker.getId());
	}
	
	public static String getOctId(){
		return Long.toOctalString(IdWorker.getId());
	}
	
	public static String getHexId(){
		return Long.toHexString(IdWorker.getId());
	}
	
	public static String getDtmId(){
		return Long.toString(IdWorker.getId(), 32);
	}
	
	public static String getRadixId(int radix){
		return Long.toString(IdWorker.getId(), radix);
	}
	
	
	
	public static void main(String[] args) throws Exception {
	 long s = 999999999999999999L;
	System.out.println(s);
	System.out.println(Long.toBinaryString(s));
	System.out.println(Long.toOctalString(s));
	System.out.println(Long.toHexString(s));
	System.out.println(Long.toString(s, 32));
	}


}
