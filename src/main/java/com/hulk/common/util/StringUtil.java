package com.hulk.common.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/**
 * @author 
 * 
 */
public class StringUtil {
	
	
	/**
	 * 字符串是否为空白 空白的定义如下： <br>
	 * 1、为null <br>
	 * 2、为不可见字符（如空格）<br>
	 * 3、""<br>
	 * 
	 * @param str 被检测的字符串
	 * @return 是否为空
	 */
	public static boolean isBlank(String str) {
		int length;
		if ((str == null) || ((length = str.length()) == 0)) {
			return true;
		}
		for (int i = 0; i < length; i++) {
			// 只要有一个非空字符即为非空字符串
			if (false == Character.isWhitespace(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	/**
	 * 将字符串的首字符变成大写字符
	 * 
	 * @param s
	 * @return
	 */
	public static String firstCharUpper(String s) {
		String result = s.substring(0, 1).toUpperCase() + s.substring(1);
		return result;
	}

	/**
	 * 将字符串的首字符变成小写字符
	 * 
	 * @param s
	 * @return
	 */
	public static String firstCharLower(String s) {
		String result = s.substring(0, 1).toLowerCase() + s.substring(1);
		return result;
	}

	/**
	 * 大写首字母<br>
	 * 例如：str = name, return Name
	 * 
	 * @param str 字符串
	 * @return 字符串
	 */
	public static String upperFirst(String str) {
		return Character.toUpperCase(str.charAt(0)) + str.substring(1);
	}

	/**
	 * 小写首字母<br>
	 * 例如：str = Name, return name
	 * 
	 * @param str 字符串
	 * @return 字符串
	 */
	public static String lowerFirst(String str) {
		if(isBlank(str)){
			return str;
		}
		return Character.toLowerCase(str.charAt(0)) + str.substring(1);
	}

	/**
	 * 生成指定长度的随机字符串（0--9,A--Z,a--Z）
	 * 
	 * 
	 * 
	 * 
	 * @param length
	 * @return
	 */
	public static String genRandom(int length) {
		StringBuffer buffer = new StringBuffer();
		Random r = new Random();
		int i = 0;
		int c;
		while (i < length) {
			c = r.nextInt(122);
			if ((48 <= c && c <= 57) || (65 <= c && c <= 90)
					|| (97 <= c && c <= 122)) {
				buffer.append((char) c);
				i++;
			}
		}
		return buffer.toString();
	}

	/**
	 * 字符串左边补零
	 * 
	 * 
	 * 
	 * 
	 * 例：将字符串"ABC"用"0"补足8位，结果为"00000ABC"
	 * 
	 * @param orgStr
	 *            原始字符串
	 * 
	 * 
	 * 
	 * 
	 * @param fillWith
	 *            用来填充的字符
	 * 
	 * 
	 * 
	 * 
	 * @param fixLen
	 *            固定长度
	 * @return
	 */
	public static String fillLeft(String orgStr, char fillWith, int fixLen) {

		return fillStr(orgStr, fillWith, fixLen, true);

	}

	/**
	 * 字符串右边补零
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param orgStr
	 * @param fillWith
	 * @param fixLen
	 * @return
	 */
	public static String fillRight(String orgStr, char fillWith, int fixLen) {

		return fillStr(orgStr, fillWith, fixLen, false);

	}

	private static String fillStr(String orgStr, char fillWith, int fixLen,
			boolean isLeft) {

		int toFill = fixLen - orgStr.length();

		if (toFill <= 0)
		{return orgStr;}

		StringBuilder sb = new StringBuilder(orgStr);
		for (; toFill > 0; toFill--) {
			if (isLeft)
			{sb.insert(0, fillWith);}
			else
			{sb.append(fillWith);}
		}

		return sb.toString();

	}

	/**
	 * toTrim
	 * 
	 * @param str
	 * @return
	 */
	public static String toTrim(String str) {
		if (str == null) {
			return "";
		}
		return str.trim();
	}

	public static String convertToString(int length, int value) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < length - ("" + value).length(); i++) {
			buffer.append("0");
		}
		buffer.append(value);
		return buffer.toString();
	}

	public static String arrayToString(Object[] array, String split) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			buffer.append(array[i].toString()).append(split);
		}
		if (buffer.length() != 0) {
			return buffer.substring(0, buffer.length() - split.length());
		} else {
			return "";
		}
	}

	@SuppressWarnings("rawtypes")
	public static String arrayToString(Set set, String split) {
		StringBuffer buffer = new StringBuffer();
		for (Iterator i = set.iterator(); i.hasNext();) {
			buffer.append(i.next().toString()).append(split);
		}
		if (buffer.length() != 0) {
			return buffer.substring(0, buffer.length() - split.length());
		} else {
			return "";
		}
	}

	public static String trimLeft(String s, char c) {
		if (s == null) {
			return "";
		} else {
			StringBuffer b = new StringBuffer();
			char[] cc = s.toCharArray();
			int i = 0;
			for (i = 0; i < cc.length; i++) {
				if (cc[i] != c) {
					break;
				}
			}
			for (int n = i; n < cc.length; n++) {
				b.append(cc[n]);
			}
			return b.toString();
		}
	}

	public static String repNull(Object o) {
		if (o == null) {
			return "";
		} else {
			return o.toString().trim();
		}
	}

	public static String generateRandomString(int len) {
		final char[] mm = new char[] { '0', '1', '2', '3', '4', '5', '6', '7',
				'8', '9' };

		StringBuffer sb = new StringBuffer();
		Random random = new Random();

		for (int i = 0; i < len; i++) {
			sb.append(mm[random.nextInt(mm.length)]);
		}
		return sb.toString();

	}

	public static String toColumnName(String attributeName) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < attributeName.length(); i++) {
			char temp = attributeName.charAt(i);
			if (temp >= 'a' && temp <= 'z') {
				buffer.append((char) ((int) temp - 32));
			}
			if (temp >= 'A' && temp <= 'Z') {
				buffer.append("_").append(temp);
			}
		}
		return buffer.toString();
	}

	public static String toPropertyName(String columnName) {
		StringBuffer buffer = new StringBuffer();
		boolean b = false;
		for (int i = 0; i < columnName.length(); i++) {
			char temp = columnName.charAt(i);
			if (temp >= '0' && temp <= '9') {
				buffer.append((char) ((int) temp));
			} else if (temp == '_') {
				b = true;
			} else {
				if (!b) {
					buffer.append((char) ((int) temp + 32));
				} else {
					buffer.append((char) ((int) temp));
				}
				b = false;
			}
		}
		return buffer.toString();
	}

	/**
	 * 
	 * Convert byte[] to hex
	 * string.这里我们可以将byte转换成int，然后利用Integer.toHexString(int)来转换成16进制字符串。
	 * 
	 * @param src
	 *            byte[] data
	 * @return hex string
	 */

	public static String bytesToHexString(byte[] src) {

		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();

	}

	/**
	 * Convert hex string to byte[]
	 * 
	 * @param hexString
	 *            the hex string
	 * @return byte[]
	 */
	public static byte[] hexStringToBytes(String hexString) {
		if (hexString == null || hexString.equals("")) {
			return null;
		}
		hexString = hexString.toUpperCase();
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toCharArray();
		byte[] d = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
		}
		return d;
	}

	/**
	 * Convert char to byte
	 * 
	 * @param c
	 *            char
	 * @return byte
	 */

	private static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

	public static String xor(String s1, String s2) {
		int c = Integer.parseInt(s1);
		int d = Integer.parseInt(s2);
		int e = c ^ d;
		String f = Integer.toString(e);
		return f;
	}

	

	public static String converStringToDate(String textStr) {
		if (textStr == null) {
			return "";
		} else {
			return textStr.substring(0, 4) + "-" + textStr.substring(4, 6)
					+ "-" + textStr.substring(6, 8) + " "
					+ textStr.substring(8, 10) + ":"
					+ textStr.substring(10, 12) + ":"
					+ textStr.substring(12, 14);
		}
	}

	public static String nvl(String str, String repal) {
		if (str == null || str.equals(""))
		{return repal;}
		return "";
	}

	/**
	 * 验证一个字符串是否是数字组成
	 * 
	 * @param s
	 *            要验证的字符串
	 * @return 如果字符串是数字组成的则返回true,否则返回false
	 */
	public static boolean isNumber(String s) {
		if (s == null || s.equals(""))
		{return false;}
		String num = "0123456789";
		for (int i = 0; i < s.length(); i++) {
			if (num.indexOf(s.charAt(i)) < 0)
			{return false;}
		}
		return true;
	}

	public static int length(String s) {
		if (s == null) {
			return 0;
		} else {
			return s.length();
		}
	}

	public static String[] splitToArray(String s, int length) {
		if (s == null || s.length() == 0) {
			return null;
		}
		int segs = (s.length() - 1) / length + 1;
		String[] arr = new String[segs];
		int i = 0;
		int n = 0;
		StringBuffer b = new StringBuffer();
		while (i < s.length()) {
			b.append(s.charAt(i));
			i++;
			if (b.length() == length) {
				arr[n] = b.toString();
				b = new StringBuffer();
				n++;
			}
		}
		if (b.length() != 0) {
			arr[segs - 1] = b.toString();
		}
		return arr;
	}


	public static String replaceString(String oldString, String... arg) {
		for (String replaceStr : arg) {
			oldString = oldString.replaceFirst("#", replaceStr);

		}
		return oldString;

	}

	public static String formatNumber(double paramDouble, int paramInt) {
		return NumberFormat.getNumberInstance().format(
				round(paramDouble, paramInt));
	}

	public static double round(double paramDouble, int paramInt) {
		if (paramInt < 0) {
			throw new RuntimeException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal localBigDecimal1 = new BigDecimal(
				Double.toString(paramDouble));
		BigDecimal localBigDecimal2 = new BigDecimal("1");
		return localBigDecimal1.divide(localBigDecimal2, paramInt, 4)
				.doubleValue();
	}
	
	
	
}
