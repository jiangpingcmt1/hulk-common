
package com.hulk.common.util;

import java.nio.charset.StandardCharsets;

/**
 * @author cmt
 * @E-mail:29572320@qq.com
 * @version Create on: 2017年6月1日 下午3:30:10 Class description
 */

public class CharsetsUtil {

	public static byte[] getUTF8Bytes(String input) {
		return input.getBytes(StandardCharsets.UTF_8);
	}

}
