package com.hulk.common.util;

import java.util.zip.CRC32;

/**
 * @Author: hulk
 * @E-mail: 29572320@qq.com
 * @Date: Create in 2018/8/11 下午1:59
 * @Version: 1.0.0
 * @Modify by:
 * Class description
 */
public class CRC32Util {
        public static long crc32Code(byte[] bytes) {
            CRC32 crc32 = new CRC32();
            crc32.update(bytes);
            return crc32.getValue();
        }

}
