package com.hulk.common.util.deprecated;


/**
 * @project Posp
 * @description �ֽڹ�����
 */
public class ByteUtil {

	public static final int UNIT_LENGTH = 8;

	/**
	 * ȡbyte�����һ���ַ���
	 * 
	 * @param b
	 *            ��ȡ��byte����
	 * @param start
	 *            ��ʼλ��
	 * @param end
	 *            ����λ��
	 * @return
	 */
	public static byte[] subByte(byte b[], int start, int end) {

		int sublength = end - start;
		byte[] result = new byte[sublength];

		for (int i = 0; i < sublength; i++) {
			result[i] = b[start + i];
		}

		return result;
	}

	/**
	 * ��byte������xor����
	 * 
	 * @param b1
	 *            byte���飬����Ҫ��Ϊ8
	 * @param b2
	 *            byte���飬����Ҫ��Ϊ8 ����Des������������Ϊ64λ�����Ա������涨b1��b2�ĳ��ȱ���Ϊ8
	 * 
	 * @return ����byte����xor��Ľ��
	 * 
	 * @exception IllegalArgumentException
	 *                when ����b1��b2�ĳ��� != 8
	 */
	public static byte[] doXor(byte[] b1, byte[] b2) {

		int byte_length = 8;

		byte[] result = new byte[byte_length];

		if (b1.length != byte_length || b2.length != byte_length) {
			throw new IllegalArgumentException(
					"Both byte array'length must = 8!");
		}

		for (int i = 0; i < b1.length; i++) {
			result[i] = doXor(b1[i], b2[i]);
		}

		return result;

	}

	/**
	 * ��byte��xor����
	 * 
	 * @param b1
	 * @param b2
	 * @return ��������byte xor��Ľ��
	 */
	public static byte doXor(byte b1, byte b2) {
		return (byte) (b1 ^ b2);
	}

	/**
	 * ��byte���鳤��������8�ı��� ����Ĳ����ö�����0���
	 * 
	 * @param bs
	 *            ��Ҫ���������
	 * @return
	 */
	public static byte[] appendTo8Multiple(byte bs[]) {
		
		int length = bs.length;
		int gap = 0;

		if (length % UNIT_LENGTH != 0) {
			gap = UNIT_LENGTH - length % UNIT_LENGTH;
		}

		int newlength = length + gap;

		byte rs[] = new byte[newlength];

		for (int i = 0; i < bs.length; i++) {
			rs[i] = bs[i];
		}

		return rs;
	}
	
	public static byte [] padByteStart(byte bs[] ,int len ,byte c){
		byte rs[] = new byte[len];
		byte [] cs = new byte[1];
		cs[0]= (byte) c;
		//int j = 0;
	    for (int i = 0; i < len-bs.length; i++) {
		   System.arraycopy(cs, 0, rs, i, 1);
		  // j++;
	    }
	    System.arraycopy(bs, 0, rs, len-bs.length, bs.length);
		return rs;
	
	}
	

	public static byte [] padByteEnd(byte bs[] ,int len ,byte c){
		byte rs[] = new byte[len];
		byte [] cs = new byte[1];
		cs[0]= (byte) c;
		System.arraycopy(bs, 0, rs, 0, bs.length);
		
	    for (int i = bs.length; i < len; i++) {
		   System.arraycopy(cs, 0, rs, i, 1);
		  
	    }
	  
		return rs;
	
	}
	
}
