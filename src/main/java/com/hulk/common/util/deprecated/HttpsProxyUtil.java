package com.hulk.common.util.deprecated;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.contrib.ssl.AuthSSLProtocolSocketFactory;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.protocol.Protocol;

@Slf4j
public class HttpsProxyUtil {
	
	
	public static String sendByHttpsProxy(String proxyHost, int proxyPort,
			String remoteUrl, String remoteMethod, int remotePort,
			String keystoreDir, String keystorePass, String truststoreDir,
			String truststorePass, Map<String, String> params){
		
		return sendByHttpsProxy( proxyHost,  proxyPort,
				 remoteUrl,  remoteMethod,  remotePort,
				 keystoreDir,  keystorePass,  truststoreDir,
				 truststorePass,   60000,  60000 , params);
		
	}

	@SuppressWarnings("deprecation")
	public static String sendByHttpsProxy(String proxyHost, int proxyPort,
			String remoteUrl, String remoteMethod, int remotePort,
			String keystoreDir, String keystorePass, String truststoreDir,
			String truststorePass,  int connectionTimeout, int timeout , Map<String, String> params)
			{
		HttpClient client = new HttpClient();
		PostMethod pm = new PostMethod(remoteMethod);
		try {
			Protocol	myhttps = new Protocol("https", new AuthSSLProtocolSocketFactory(
					new URL("file:///" + keystoreDir), keystorePass, new URL(
							"file:///" + truststoreDir), truststorePass),
					remotePort);
			client.getHostConfiguration().setProxy(proxyHost, proxyPort);
			client.getHostConfiguration().setHost(remoteUrl, remotePort, myhttps);
			client.setConnectionTimeout(connectionTimeout);
			client.setTimeout(timeout);
			// client.getParams().setAuthenticationPreemptive(true);
			
			for (Map.Entry<String, String> entry : params.entrySet()) {
				pm.setParameter(entry.getKey(), entry.getValue());
			}
			int re_code = client.executeMethod(pm);
			log.info("HttpStatus:{}", re_code);
			if (re_code == HttpStatus.SC_OK) {
				return pm.getResponseBodyAsString();
			} else {
				log.error("发送失败,https错误码:{}", re_code);
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("",e);
			throw new RuntimeException(e);
		}finally{
			try {
				pm.releaseConnection();
			} catch (Exception e) {
				e.printStackTrace();
				log.error("",e);
			}
		
		}

	}

	
	public static String sendByHttpProxy(String proxyHost, int proxyPort,
			String remoteUrl, String remoteMethod, String protocol,
			int remotePort,   Map<String, String> params)
	{
		return sendByHttpProxy( proxyHost,  proxyPort,
				 remoteUrl,  remoteMethod,  protocol,
				 remotePort,  60000,  60000 ,   params);
	}
	
	//ip+port+remoteMethod
	@SuppressWarnings("deprecation")
	public static String sendByHttpProxy(String proxyHost, int proxyPort,
			String remoteUrl, String remoteMethod, String protocol,
			int remotePort, int connectionTimeout, int timeout ,  Map<String, String> params) {
		HttpClient client = new HttpClient();
		PostMethod  pm = new PostMethod(remoteMethod);;
		try {
			
			client.getHostConfiguration().setProxy(proxyHost, proxyPort);
			client.getHostConfiguration().setHost(remoteUrl, remotePort,
					protocol); // 支持http，或者https
			client.setConnectionTimeout(connectionTimeout);
			client.setTimeout(timeout);
			// client.getParams().setAuthenticationPreemptive(true);
			
			for (Map.Entry<String, String> entry : params.entrySet()) {
				pm.setParameter(entry.getKey(), entry.getValue());
			}
			int re_code = client.executeMethod(pm);
			log.info("HttpStatus:{}", re_code);
			if (re_code == HttpStatus.SC_OK) {
				return pm.getResponseBodyAsString();
			} else {
				log.error("发送失败,http错误码:{}", re_code);
				return pm.getResponseBodyAsString();
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error("",e);
			throw new RuntimeException(e);
		}finally{
			try {
				pm.releaseConnection();
			} catch (Exception e) {
				e.printStackTrace();
				log.error("",e);
			}
		
		}
		
	}
}
