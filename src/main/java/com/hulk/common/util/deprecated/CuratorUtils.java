package com.hulk.common.util.deprecated;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.CuratorFrameworkFactory.Builder;
import org.apache.curator.framework.recipes.atomic.AtomicValue;
import org.apache.curator.framework.recipes.atomic.DistributedAtomicInteger;
import org.apache.curator.framework.recipes.atomic.DistributedAtomicLong;
import org.apache.curator.framework.recipes.barriers.DistributedBarrier;
import org.apache.curator.framework.recipes.barriers.DistributedDoubleBarrier;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.curator.framework.recipes.cache.NodeCacheListener;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCache.StartMode;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListener;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.framework.recipes.shared.SharedCount;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.retry.RetryNTimes;
import org.apache.curator.utils.CloseableUtils;
import org.apache.zookeeper.CreateMode;

import com.google.common.base.Preconditions;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class CuratorUtils {
	private static final String UTF_8 =  StandardCharsets.UTF_8.name();
	private static CuratorFramework client;

	public static void init(String serverLists, int baseSleepTimeMilliseconds, int maxRetries,
			int maxSlemepTimeMilliseconds, String namespace) {
		try {
			Builder builder = CuratorFrameworkFactory
					.builder()
					.connectString(serverLists)
					.retryPolicy(
							new ExponentialBackoffRetry(baseSleepTimeMilliseconds, maxRetries,
									maxSlemepTimeMilliseconds)).namespace(namespace);
			client = builder.build();
			client.start();
			client.blockUntilConnected();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public static boolean getDistributedAtomicInteger(String path, Integer value) {
		SharedCount count = new SharedCount(client, path, 0);
		try {
			boolean result = count.trySetCount(count.getVersionedValue(), value);
			return result;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			try {
				count.close();
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
		}
		return false;
	}

	public static boolean getDistributedAtomicLong(String path, Long value) {
		DistributedAtomicLong count = new DistributedAtomicLong(client, path, new RetryNTimes(10, 10));
		try {
			AtomicValue<Long> result = count.add(value);
			if (result.succeeded()) {
				// Long preValue = result.preValue();
				// Long postValue = result.postValue();
				return result.succeeded();
			}
			return result.succeeded();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return false;
	}

	public static void close() {
		CloseableUtils.closeQuietly(client);
	}

	public String get(final String key) {
		return getDirectly(key);
	}

	public String getDirectly(final String key) {
		try {
			return new String(client.getData().forPath(key), Charset.forName(UTF_8));
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
			return null;
		}
	}

	public List<String> getChildrenKeys(final String key) {
		try {
			List<String> result = client.getChildren().forPath(key);
			Collections.sort(result, new Comparator<String>() {
				public int compare(final String o1, final String o2) {
					return o2.compareTo(o1);
				}
			});
			return result;
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
			return Collections.emptyList();
		}
	}

	public static boolean isExisted(final String key) {
		try {
			return null != client.checkExists().forPath(key);
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
			return false;
		}
	}

	public static void persist(final String key, final String value) {
		try {
			if (!isExisted(key)) {
				create(key, value);
			} else {
				update(key, value);
			}
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
		}
	}

	public static void create(final String key, final String value) throws Exception {
		client.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath(key, value.getBytes());
	}

	public static void update(final String key, final String value) {
		try {
			client.inTransaction().check().forPath(key).and().setData()
					.forPath(key, value.getBytes(Charset.forName(UTF_8))).and().commit();
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
		}
	}

	public static void persistEphemeral(final String key, final String value) {
		try {
			if (isExisted(key)) {
				client.delete().deletingChildrenIfNeeded().forPath(key);
			}
			client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL)
					.forPath(key, value.getBytes(Charset.forName(UTF_8)));
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
		}
	}

	public void persistEphemeralSequential(final String key) {
		try {
			client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL_SEQUENTIAL).forPath(key);
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
		}
	}

	public void remove(final String key) {
		try {
			client.delete().deletingChildrenIfNeeded().forPath(key);
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
		}
	}

	public long getRegistryCenterTime(final String key) {
		long result = 0L;
		try {
			String path = client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL_SEQUENTIAL)
					.forPath(key);
			result = client.checkExists().forPath(path).getCtime();
			// CHECKSTYLE:OFF
		} catch (final Exception ex) {
			// CHECKSTYLE:ON
		log.error("",ex);
		}
		Preconditions.checkState(0L != result, "Cannot get registry center time.");
		return result;
	}

	public static CuratorFramework getClient() {
		return client;
	}

	public static void setClient(CuratorFramework client) {
		CuratorUtils.client = client;
	}

	public static void NodeCache(String path) {
		try {
			final NodeCache nodeCache = new NodeCache(client, path, false);
			nodeCache.start();
			nodeCache.getListenable().addListener(new NodeCacheListener() {

				public void nodeChanged() throws Exception {
					log.info(nodeCache.getCurrentData()!=null?"":nodeCache.getCurrentData().toString());
				}
			});
			nodeCache.close();
		} catch (Exception e) {
			log.error("",e);
		}
	}

	public static void PathChildrenCache(String path) {
		try {
			PathChildrenCache cache = new PathChildrenCache(client, path, true);
			cache.getListenable().addListener(new PathChildrenCacheListener() {
				public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
					switch (event.getType()) {
					case CHILD_ADDED:
						log.info(event.getData().getPath());
						break;
					case CHILD_UPDATED:
						log.info(event.getData().getPath());
						break;
					case CHILD_REMOVED:
						log.info(event.getData().getPath());
						break;
					default:
						break;
					}
				}
			});
			cache.start(StartMode.POST_INITIALIZED_EVENT);
			cache.close();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public static void LeaderSelector(String path) {
		LeaderSelector leaderSelector = new LeaderSelector(client, path, new LeaderSelectorListener() {
			public void stateChanged(CuratorFramework arg0, ConnectionState arg1) {
				log.info("stateChanged");
			}

			public void takeLeadership(CuratorFramework client) throws Exception {
				log.info("takeLeadership");
			}
		});
		leaderSelector.autoRequeue();
		leaderSelector.start();
		leaderSelector.close();
	}

	static int l = 0;

	public static void DistributedLock(String path) {
		InterProcessMutex lock = new InterProcessMutex(client, path);
		try {
			lock.acquire();
			lock.release();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public static AtomicValue<Integer> distributedInteger(String path, Integer data) {
		DistributedAtomicInteger atomicInteger = new DistributedAtomicInteger(client, path, new RetryNTimes(3, 100));
		try {
			AtomicValue<Integer> result = atomicInteger.add(data);
			int preValue = result.preValue();
			int postValue = result.postValue();
			return result;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public static void DistributedBarrier(String path, Integer data) {
		DistributedBarrier barrier = new DistributedBarrier(client, path);
		try {
			barrier.setBarrier();
			barrier.waitOnBarrier();// 等待barrier为0
			barrier.removeBarrier();// 清空计数器
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public static void DistributedDoubleBarrier(String path, Integer data) {
		DistributedDoubleBarrier barrier = new DistributedDoubleBarrier(client, path, 5);
		try {
			barrier.enter();
			barrier.leave();// 清空计数器
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public static void getAllPath(String servers, String namespace){
		try {
			CuratorUtils.init(servers, 100, 3, 300, namespace);
			getJobDetail("/");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public static void getJobDetail(String path) throws Exception {
		CuratorFramework client = CuratorUtils.getClient();
		List<String> childrens  = client.getChildren().forPath(path);
		for(String children : childrens){
			String subPath = path +"/"+ children;
			if(!path .equals("/")){
				subPath = path +"/"+ children;
			}else{
				subPath = "/"+ children;
			}
			
			String value = new String(client.getData().forPath(subPath)).toString();
			log.info(subPath + " | " + value);
			
			getJobDetail(subPath);
		}
	}

	public static void main(String[] args){
		getAllPath("10.199.182.60:2181","xxxxxxxxxxxx");
	}

}
