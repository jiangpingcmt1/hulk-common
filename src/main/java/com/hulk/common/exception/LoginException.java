package com.hulk.common.exception;


import com.hulk.common.support.http.HttpCode;

@SuppressWarnings("serial")
public class LoginException extends BaseException {
	public LoginException() {
	}

	public LoginException(String message) {
		super(message);
	}

	public LoginException(String message, Exception e) {
		super(message, e);
	}
@Override
	protected HttpCode getCode() {
		return HttpCode.LOGIN_FAIL;
	}
}
