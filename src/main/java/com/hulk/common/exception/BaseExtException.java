package com.hulk.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 
 * @version 2014-1-21 下午10:53:55 
 */
public class BaseExtException extends RuntimeException {

	
	private static final long serialVersionUID = -1L;
	/** 错误结果代码 */
	@Getter
	@Setter
	private String errorCode;
	/** 错误信息 */
	@Getter
	@Setter
	private String errorMessage;
	
	/** 内部交易代码 */
	@Getter
	@Setter
	protected String intTxnSeq;

	/*业务信息，可能为空，参见com.hulk.aggregationcenter.persistence.entity.TLogPubErrorRecord*/
	@Getter
	@Setter
	protected String errorRecordJson;


	/**
	 * 构造函数
	 * @param errorCode
	 *            错误代码
	 * @param throwable
	 *            Throwable
	 */
	public BaseExtException(String errorCode, Throwable throwable) {
		super(throwable.getMessage());
		this.errorCode = errorCode;
		this.errorMessage = throwable.getMessage();
	}

	/**
	 * 构造函数
	 * @param errorCode
	 *            错误代码
	 * @param errorMessage
	 *            错误信息
	 */
	public BaseExtException(String errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	/**
	 * 构造函数
	 * @param errorCode
	 *            错误代码
	 * @param errorMessage
	 *            错误信息
	 * @param throwable
	 */
	public BaseExtException(String errorCode, String errorMessage,
							Throwable throwable) {
		super(errorMessage, throwable);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	
}
