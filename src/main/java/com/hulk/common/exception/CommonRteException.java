package com.hulk.common.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * cmt
 */
@Slf4j
public class CommonRteException extends BaseExtException {


	private static final long serialVersionUID = 1L;
	
	
	/**
	 * @param errorCode
	 * @param errorMessage
	 */
	public CommonRteException(String intTxnSeq, String errorCode, String errorMessage) {
		super(errorCode, errorMessage);
		this.intTxnSeq = intTxnSeq;
		log.error("intTxnSeq:{},errorCode:{},errorMessage:{}", new Object[] {
				intTxnSeq, errorCode, errorMessage });
	}

	/**
	 * @param errorCode
	 * @param throwable
	 */
	@Deprecated
	public CommonRteException(String intTxnSeq, String errorCode, Throwable throwable) {
		super(errorCode, throwable);
		this.intTxnSeq = intTxnSeq;
		log.error("intTxnSeq:{},errorCode:{},throwable:{}", new Object[] {
				intTxnSeq, errorCode, throwable.getMessage() });
	}

	/**
	 * @param errorCode
	 * @param errorMessage
	 * @param throwable
	 */
	public CommonRteException(String intTxnSeq, String errorCode, String errorMessage,
			Throwable throwable) {
		super(errorCode, errorMessage, throwable);
		this.intTxnSeq = intTxnSeq;
		log.error(
				"intTxnSeq:{},errorCode:{},errorMessage:{},throwable:{}",
				new Object[] { intTxnSeq, errorCode, errorMessage,
						throwable.getMessage() });
	}
	
}
