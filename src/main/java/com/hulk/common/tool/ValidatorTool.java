package com.hulk.common.tool;


import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.resourceloading.AggregateResourceBundleLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Objects;




/**
 * @author cmt
 * @version 2014-2-27 上午1:15:24 
 */
public class ValidatorTool {
	private static final Logger log = LoggerFactory.getLogger(ValidatorTool.class);
	
//  private static ValidatorTool instance = null;
//	private static Validator validator = null;
//	private static class Holder { // lazy class
//		static final ValidatorTool ValidatorTool = new ValidatorTool();
//	}
//	public static ValidatorTool getInstance() {
//		return Holder.ValidatorTool;
//	}
//		private ValidatorTool() {
//	validator =	Validation.buildDefaultValidatorFactory().getValidator();
//}


	

//	private static ValidatorTool ValidatorTool = null;
//	private static Validator validator = null;
//
//	public static ValidatorTool getInstance() {
//		if (ValidatorTool == null || validator == null) {
//			ValidatorTool = new ValidatorTool();
//			validator = Validation.buildDefaultValidatorFactory()
//					.getValidator();
//		}
//		return ValidatorTool;
//
//	}

	/**
	 * Hide Utility Class Constructor
	 */
//	private ValidatorTool() {
//	}

	private static Validator validator = null;
//	static {
//		loadValidatorInstance();
//	}
	
	private synchronized static void loadValidatorInstance() {
		log.info("validator begin to load begin only one");
//		if (validator == null) {
//			log.info("validator begin to load instance");
//			validator = Validation.buildDefaultValidatorFactory()
//					.getValidator();
//			log.info("validator load instance over");
//		}
		
//		Validator validator = Validation.byDefaultProvider().configure()
//				.messageInterpolator(
//						new ResourceBundleMessageInterpolator(new PlatformResourceBundleLocator("MyMessages")))
//				.buildValidatorFactory().getValidator();

		if (validator == null) {
			log.info("validator begin to load instance");
			validator = Validation.byDefaultProvider().configure()
					.messageInterpolator(new ResourceBundleMessageInterpolator(
							new AggregateResourceBundleLocator(Arrays.asList("messages","messages_zh_CN"))))
					.buildValidatorFactory().getValidator();
			log.info("validator load instance over");
		}
		
	}
	
	
	// 1.@AssertTrue //用于boolean字段，该字段只能为true
	// 2.@AssertFalse//该字段的值只能为false
	// 3.@CreditCardNumber//对信用卡号进行一个大致的验证
	// 4.@DecimalMax//只能小于或等于该值
	// 5.@DecimalMin//只能大于或等于该值
	// 6.@Digits(integer=2,fraction=20)//检查是否是一种数字的整数、分数,小数位数的数字。
	// 7.@Email//检查是否是一个有效的email地址
	// 8.@Future//检查该字段的日期是否是属于将来的日期
	// 9.@Length(min=,max=)//检查所属的字段的长度是否在min和max之间,只能用于字符串
	// 10.@Max//该字段的值只能小于或等于该值
	// 11.@Min//该字段的值只能大于或等于该值
	// 12.@NotNull//不能为null
	// 13.@NotBlank//不能为空，检查时会将空格忽略
	// 14.@NotEmpty//不能为空，这里的空是指空字符串
	// 15.@Null//检查该字段为空
	// 16.@Past//检查该字段的日期是在过去
	// 17.@Size(min=, max=)//检查该字段的size是否在min和max之间，可以是字符串、数组、集合、Map等
	// 18.@URL(protocol=,host,port)//检查是否是一个有效的URL，如果提供了protocol，host等，则该URL还需满足提供的条件
	// 19.@Valid//该注解只要用于字段为一个包含其他对象的集合或map或数组的字段，或该字段直接为一个其他对象的引用，
	// 20. //这样在检查当前对象的同时也会检查该字段所引用的对象

	public static String validateBean(Object obj) {// 验证某一个对象
		if (validator == null) {
			loadValidatorInstance();
		}
		StringBuilder buffer = new StringBuilder(64);// 用于存储验证后的错误信息
		Set<ConstraintViolation<Object>> constraintViolations = validator
				.validate(obj);// 验证某个对象,，其实也可以只验证其中的某一个属性的

		Iterator<ConstraintViolation<Object>> iter = constraintViolations
				.iterator();
		while (iter.hasNext()) {
			String message = iter.next().getMessage();
			buffer.append(message);
		}
		String	valResVal =	buffer.toString().trim();
		if(!Objects.equal("", valResVal)){
			log.info("validator error: "+valResVal);
		}
		return valResVal;
	}

}
