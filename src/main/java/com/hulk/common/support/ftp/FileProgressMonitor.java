package com.hulk.common.support.ftp;

import com.jcraft.jsch.SftpProgressMonitor;
import lombok.extern.slf4j.Slf4j;


import java.text.DecimalFormat;

/**
 * 监控
 * 
 * @author hulk
 * @version 2016年5月20日 下午3:19:19
 */
@Slf4j
public class FileProgressMonitor implements SftpProgressMonitor {
   
    private long transfered; // 记录已传输的数据总大小
    private long fileSize; // 记录文件总大小
    private int minInterval = 100; // 打印日志时间间隔
    private long start; // 开始时间
    private DecimalFormat df = new DecimalFormat("#.##");
    private long preTime;

    /** 传输开始 */
    @Override
    public void init(int op, String src, String dest, long max) {
        this.fileSize = max;
        log.info("Transferring begin.");
        start = System.currentTimeMillis();
    }

    /** 传输中 */
    @Override
    public boolean count(long count) {
        if (fileSize != 0 && transfered == 0) {
            log.info("Transferring progress message: {}%", df.format(0));
            preTime = System.currentTimeMillis();
        }
        transfered += count;
        if (fileSize != 0) {
            long interval = System.currentTimeMillis() - preTime;
            if (transfered == fileSize || interval > minInterval) {
                preTime = System.currentTimeMillis();
                double d = ((double)transfered * 100) / (double)fileSize;
                log.info("Transferring progress message: {}%", df.format(d));
            }
        } else {
            log.info("Transferring progress message: " + transfered);
        }
        return true;
    }

    /** 传输结束 */
    @Override
    public void end() {
        log.info("Transferring end. used time: {}ms", String.valueOf(System.currentTimeMillis() - start));
    }
}
