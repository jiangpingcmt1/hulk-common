package com.hulk.common.support.mq.feature;

import javax.jms.Message;
import javax.jms.MessageListener;

import lombok.extern.slf4j.Slf4j;


/**
 * @author 
 *
 */
@Slf4j
public class JmsClientRecvListener implements MessageListener {
	

	/**
	 * client templeate
	 */
	private IClientSendMessage clientrecv;

	public IClientSendMessage getClientrecv() {
		return clientrecv;
	}

	public void setClientrecv(IClientSendMessage c) {
		this.clientrecv =c;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	public void onMessage(Message message) {
	
		try {
			log.debug("JmsClientRecvListener-onMessage start");
			clientrecv.recvmessagefromserver(message);
			log.debug("JmsClientRecvListener-onMessage end");
		} catch (Exception e) {
			log.error("JmsClientRecvListener-onMessage",e);

		}

	}
}
