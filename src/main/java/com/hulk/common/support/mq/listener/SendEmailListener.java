package com.hulk.common.support.mq.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.hulk.common.support.file.email.Email;
import com.hulk.common.util.EmailUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 发送邮件队列
 * 
 * @author hulk
 * @version 2016年6月14日 上午11:00:53
 */
@Slf4j
public class SendEmailListener implements MessageListener {

	@Override
	public void onMessage(Message message) {
		try {
			Email email = (Email) ((ObjectMessage) message).getObject();
			log.info("将发送邮件至：" + email.getSendTo());
			EmailUtil.sendEmail(email);
		} catch (JMSException e) {
			log.error("",e);
		}
	}
}
