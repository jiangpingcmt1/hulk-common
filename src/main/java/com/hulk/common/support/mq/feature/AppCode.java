package com.hulk.common.support.mq.feature;

import com.hulk.common.util.UUIDGenerator;

/**
 * @author
 * 
 */
public interface AppCode {

	
	
	String INST_ID = UUIDGenerator.generateUUID();
	String REQTXNSEQ = "reqTxnSeq";
	String SENDERID = "senderid";
	String DEST_SENDERID="destsenderid";
	String ISASYNC = "isaSync";
	String DEFAULTQUEUE = "9999";

}
