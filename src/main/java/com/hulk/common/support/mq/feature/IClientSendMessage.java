package com.hulk.common.support.mq.feature;

import java.io.Serializable;

import javax.jms.Message;

/**
 * @author cmt
 * 
 */
public interface IClientSendMessage {

	public void recvmessagefromserver(Message recvmessage);

	/**
	 * 无返回的消息--字符串
	 */
	public void aSyncSendMsg(final String seq, final String reqQueueName,
			final String context, final String... expand);

	public void aSyncSendMsg(final String seq, final String reqQueueName,
			final String context, final String senderid, final String... expand);

	/**
	 * 无返回的消息--对象
	 */
	public void aSyncSendMsg(final String seq, final String reqQueueName,
			final Serializable obj, final String... expand);

	public void aSyncSendMsg(final String seq, final String reqQueueName,
			final Serializable obj, final String senderid,
			final String... expand);

	public Message sendMsg(final String seq, final String reqQueueName,
			final String rcvQueueName, final String context,
			final String... expand);

	public Message sendMsg(final String seq, final String reqQueueName,
			final String rcvQueueName, final String context,
			final String senderid, final String destsenderid,
			final String... expand);

	public Message sendMsg(final String seq, final String reqQueueName,
			final String rcvQueueName, final Serializable obj,
			final String... expand);

	public Message sendMsg(final String seq, final String reqQueueName,
			final String rcvQueueName, final Serializable obj,
			final String senderid, final String destsenderid,
			final String... expand);

	public String sendTextRtnMessage(final String seq,
			final String reqQueueName, final String rcvQueueName,
			final String context);

	public Serializable sendSerializableRtnMessage(final String seq,
			final String reqQueueName, final String rcvQueueName,
			final Serializable obj);

	public String sendTextRtnMessage(final String seq,
			final String reqQueueName, final String rcvQueueName,
			final String context, final String senderid,
			final String destsenderid);

	public Serializable sendSerializableRtnMessage(final String seq,
			final String reqQueueName, final String rcvQueueName,
			final Serializable obj, final String senderid,
			final String destsenderid);
}
