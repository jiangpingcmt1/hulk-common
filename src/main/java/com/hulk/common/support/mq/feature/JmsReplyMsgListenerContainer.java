/**
 * 
 */
package com.hulk.common.support.mq.feature;

import lombok.extern.slf4j.Slf4j;

import org.springframework.jms.listener.DefaultMessageListenerContainer;



/**
 * @author 
 *
 */
@Slf4j
public class JmsReplyMsgListenerContainer extends DefaultMessageListenerContainer {

	private String selector = "senderid='" + AppCode.INST_ID + "'";
	/**
	 * 
	 */
	public JmsReplyMsgListenerContainer() {
		
		super();
		log.debug("JmsReplyMsgListenerContainer-start");
		//设置过滤器
		setMessageSelector(selector);
		log.debug("JmsReplyMsgListenerContainer-end");
	}

}
