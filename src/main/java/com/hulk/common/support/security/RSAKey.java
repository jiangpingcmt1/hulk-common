
package com.hulk.common.support.security;

import cn.hutool.core.util.StrUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.pkcs.RSAPrivateKeyStructure;
import org.bouncycastle.asn1.x509.RSAPublicKeyStructure;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.*;
import java.util.Enumeration;


/**
* @author cmt  
* @E-mail:29572320@qq.com
* @version Create on:  2017年4月28日 上午8:51:42
* Class description
*/

public class RSAKey {
	
	
	private static final String KEY_ALGORITHM ="RSA";





	public static PublicKey getRSAPublicKeyByStrSuffix(String context, String fileSuffix)
	{
		return getRSAPublicKeyByStrSuffix( context,  fileSuffix,  KEY_ALGORITHM);
	}
	/**
	 * 获取RSA公钥对象
	 *
	 * @param context
	 *            RSA秘钥文本内容
	 * @param fileSuffix
	 *            RSA公钥名称，决定编码类型
	 * @param KEY_ALGORITHM
	 *            密钥算法
	 * @return RSA公钥对象
	 * @throws RuntimeException
	 */
	public static PublicKey getRSAPublicKeyByStrSuffix(String context, String fileSuffix, String KEY_ALGORITHM)  {
		InputStream in = null;

		String keyType = "";
		if ("crt".equalsIgnoreCase(fileSuffix) || "txt".equalsIgnoreCase(fileSuffix) ||"cer".equalsIgnoreCase(fileSuffix)) {
			keyType = "X.509";
		} else if ("pem".equalsIgnoreCase(fileSuffix)) {
			keyType = "PKCS12";
		} else if(("yljf").equalsIgnoreCase(fileSuffix)){
			keyType = "yljf";
		} else{
			keyType = "PKCS12";
		}

		try {
			context = StrUtil.replaceChars(context,"\r\n","");
			in = new ByteArrayInputStream((context.getBytes(StandardCharsets.UTF_8)));
			PublicKey pubKey = null;
			if ("X.509".equals(keyType)) {
				CertificateFactory factory = CertificateFactory.getInstance(keyType);
				Certificate cert = factory.generateCertificate(in);
				pubKey = cert.getPublicKey();
			} else if ("PKCS12".equals(keyType)) {
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				StringBuilder sb = new StringBuilder();
				String readLine = null;
				while ((readLine = br.readLine()) != null) {
					if (readLine.charAt(0) == '-') {
						continue;
					} else {
						sb.append(readLine);
						sb.append('\r');
					}
				}
				X509EncodedKeySpec pubX509 = new X509EncodedKeySpec(Base64.decodeBase64(sb.toString()));
				KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
				pubKey = keyFactory.generatePublic(pubX509);
			}else if("yljf".equals(keyType)){
				BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
				String s = br.readLine();
				ASN1InputStream ain = new 	ASN1InputStream(hexStr2Bytes(s));
				RSAPublicKeyStructure pStruct = 	RSAPublicKeyStructure.getInstance(ain.readObject());
				RSAPublicKeySpec spec = new 	RSAPublicKeySpec(pStruct.getModulus(), 			pStruct.getPublicExponent());
				KeyFactory kf = KeyFactory.getInstance("RSA");
				if (in != null)
				{in.close();}
				return kf.generatePublic(spec);
			}

			return pubKey;
		} catch (FileNotFoundException e) {
			throw new RuntimeException("公钥路径文件不存在",e);
		} catch (CertificateException e) {
			throw new RuntimeException("生成证书文件错误",e);
		} catch (IOException e) {
			throw new RuntimeException("读取公钥异常",e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(String.format("生成密钥工厂时没有[%s]此类算法", KEY_ALGORITHM),e);
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException("生成公钥对象异常",e);
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static PrivateKey getRSAPrivateKeyByStrSuffix(String context, String fileSuffix){
		return getRSAPrivateKeyByStrSuffix( context,  fileSuffix,  null);
	}

	public static PrivateKey getRSAPrivateKeyByStrSuffix(String context, String fileSuffix, String password){
		return getRSAPrivateKeyByStrSuffix( context,  fileSuffix,  password,  KEY_ALGORITHM);
	}

	/**
	 * 获取RSA私钥对象
	 *
	 * @param context
	 *            RSA秘钥文本内容
	 * @param fileSuffix
	 *            RSA私钥名称，决定编码类型
	 * @param password
	 *            RSA私钥保护密钥
	 * @param KEY_ALGORITHM
	 *            密钥算法
	 * @return RSA私钥对象
	 * @throws RuntimeException
	 */
	public static PrivateKey getRSAPrivateKeyByStrSuffix (String context, String fileSuffix, String password, String KEY_ALGORITHM)
	{
		String keyType = "";
		if ("keystore".equalsIgnoreCase(fileSuffix)) {
			keyType = "JKS";
		} else if ("pfx".equalsIgnoreCase(fileSuffix) || "p12".equalsIgnoreCase(fileSuffix)) {
			keyType = "PKCS12";
		} else if ("jck".equalsIgnoreCase(fileSuffix)) {
			keyType = "JCEKS";
		} else if ("pem".equalsIgnoreCase(fileSuffix) || "pkcs8".equalsIgnoreCase(fileSuffix)) {
			keyType = "PKCS8";
		} else if ("pkcs1".equalsIgnoreCase(fileSuffix)) {
			keyType = "PKCS1";
		} else if ("yljf".equalsIgnoreCase(fileSuffix)) {
			keyType = "yljf";
		} else if ("ldys".equalsIgnoreCase(fileSuffix)) {
			keyType = "ldys";
		} else{
			keyType = "JKS";
		}

		InputStream in = null;
		BufferedReader br = null;
		try {
			context = StrUtil.replaceChars(context,"\r\n","");
			in = new ByteArrayInputStream((context.getBytes(StandardCharsets.UTF_8)));
			PrivateKey priKey = null;
			if ("JKS".equals(keyType) || "PKCS12".equals(keyType) || "JCEKS".equals(keyType)) {
				KeyStore ks = KeyStore.getInstance(keyType);
				if (password != null) {
					char[] cPasswd = password.toCharArray();
					ks.load(in, cPasswd);
					Enumeration<String> aliasenum = ks.aliases();
					String keyAlias = null;
					while (aliasenum.hasMoreElements()) {
						keyAlias = (String) aliasenum.nextElement();
						priKey = (PrivateKey) ks.getKey(keyAlias, cPasswd);
						if (priKey != null) {
							break;
						}
					}
				}
			}else if("yljf".equals(keyType)){
				 br = new BufferedReader(new InputStreamReader(in));
				String s = br.readLine();
				PKCS8EncodedKeySpec priPKCS8=new PKCS8EncodedKeySpec(hexStr2Bytes(s));
				KeyFactory keyf=KeyFactory.getInstance("RSA");
				PrivateKey myprikey=keyf.generatePrivate(priPKCS8);
				return myprikey;
			}else if("ldys".equals(keyType)){
				byte[] b = new byte[20480];
				in.read(b);
				PKCS8EncodedKeySpec priPKCS8=new PKCS8EncodedKeySpec(b);
				KeyFactory keyf=KeyFactory.getInstance("RSA");
				PrivateKey myprikey=keyf.generatePrivate(priPKCS8);
				return myprikey;
			}else {
				 br = new BufferedReader(new InputStreamReader(in));
				StringBuilder sb = new StringBuilder();
				String readLine = null;
				while ((readLine = br.readLine()) != null) {
					if (readLine.charAt(0) == '-') {
						continue;
					} else {
						sb.append(readLine);
						sb.append('\r');
					}
				}
				if ("PKCS8".equals(keyType)) {
					PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decodeBase64(sb.toString()));
					KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
					priKey = keyFactory.generatePrivate(priPKCS8);
				} else if ("PKCS1".equals(keyType)) {
//					RSAPrivateKeyStructure asn1PrivKey = new RSAPrivateKeyStructure((ASN1Sequence) ASN1Sequence.fromByteArray(sb.toString().getBytes()));
					RSAPrivateKeyStructure asn1PrivKey = new RSAPrivateKeyStructure((ASN1Sequence) ASN1Sequence.fromByteArray(Base64.decodeBase64(sb.toString())));
					KeySpec rsaPrivKeySpec = new RSAPrivateKeySpec(asn1PrivKey.getModulus(), asn1PrivKey.getPrivateExponent());
					KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
					priKey = keyFactory.generatePrivate(rsaPrivKeySpec);
				}
			}

			return priKey;
		} catch (FileNotFoundException e) {
			throw new RuntimeException("私钥路径文件不存在",e);
		} catch (KeyStoreException e) {
			throw new RuntimeException("获取KeyStore对象异常",e);
		} catch (IOException e) {
			throw new RuntimeException("读取私钥异常",e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("生成私钥对象异常",e);
		} catch (CertificateException e) {
			throw new RuntimeException("加载私钥密码异常",e);
		} catch (UnrecoverableKeyException e) {
			throw new RuntimeException("生成私钥对象异常",e);
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException("生成私钥对象异常",e);
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

    
    public static PublicKey getRSAPublicKeyByRelativeFileSuffix(String relativePath, String fileSuffix)
	{
		return getRSAPublicKeyByRelativeFileSuffix(relativePath, fileSuffix, KEY_ALGORITHM);
	}
    
	public static PublicKey getRSAPublicKeyByRelativeFileSuffix(String relativePath, String fileSuffix,
			String KEY_ALGORITHM) {

		Resource r = new ClassPathResource(relativePath);
		File f;
		String filePath = "";
		try {
			f = r.getFile();
			filePath = f.getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException("",e);
		}
		return getRSAPublicKeyByAbsoluteFileSuffix(filePath, fileSuffix, KEY_ALGORITHM);

	}
	
	public static PublicKey getRSAPublicKeyByAbsoluteFileSuffix(String filePath, String fileSuffix)
	{
		return getRSAPublicKeyByAbsoluteFileSuffix( filePath,  fileSuffix,  KEY_ALGORITHM);
	}






	/**
	 * 获取RSA公钥对象
	 * 
	 * @param filePath
	 *            RSA公钥路径
	 * @param fileSuffix
	 *            RSA公钥名称，决定编码类型
	 * @param KEY_ALGORITHM
	 *            密钥算法
	 * @return RSA公钥对象
	 * @throws RuntimeException
	 */
	public static PublicKey getRSAPublicKeyByAbsoluteFileSuffix(String filePath, String fileSuffix, String KEY_ALGORITHM)  {
		InputStream in = null;
		BufferedReader br = null;
		String keyType = "";
		if ("crt".equalsIgnoreCase(fileSuffix) || "txt".equalsIgnoreCase(fileSuffix) ||"cer".equalsIgnoreCase(fileSuffix)) {
			keyType = "X.509";
		} else if ("pem".equalsIgnoreCase(fileSuffix)) {
			keyType = "PKCS12";
		} else if(("yljf").equalsIgnoreCase(fileSuffix)){
			keyType = "yljf";
		} else{
			keyType = "PKCS12";
		}

		try {
			in = new FileInputStream(filePath);
			PublicKey pubKey = null;
			if ("X.509".equals(keyType)) {
				CertificateFactory factory = CertificateFactory.getInstance(keyType);
				Certificate cert = factory.generateCertificate(in);
				pubKey = cert.getPublicKey();
			} else if ("PKCS12".equals(keyType)) {
				 br = new BufferedReader(new InputStreamReader(in));
				StringBuilder sb = new StringBuilder();
				String readLine = null;
				while ((readLine = br.readLine()) != null) {
					if (readLine.charAt(0) == '-') {
						continue;
					} else {
						sb.append(readLine);
						sb.append('\r');
					}
				}
				X509EncodedKeySpec pubX509 = new X509EncodedKeySpec(Base64.decodeBase64(sb.toString()));
				KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
				pubKey = keyFactory.generatePublic(pubX509);
			}else if("yljf".equals(keyType)){
				br = new BufferedReader(new InputStreamReader(in, "utf-8"));
				String s = br.readLine();
				ASN1InputStream ain = new 	ASN1InputStream(hexStr2Bytes(s));
				RSAPublicKeyStructure pStruct = 	RSAPublicKeyStructure.getInstance(ain.readObject());
				RSAPublicKeySpec spec = new 	RSAPublicKeySpec(pStruct.getModulus(), 			pStruct.getPublicExponent());
				KeyFactory kf = KeyFactory.getInstance("RSA");
				/*if (in != null)
				  {in.close();}*/
				return kf.generatePublic(spec);
			}

			return pubKey;
		} catch (FileNotFoundException e) {
			throw new RuntimeException("公钥路径文件不存在",e);
		} catch (CertificateException e) {
			throw new RuntimeException("生成证书文件错误",e);
		} catch (IOException e) {
			throw new RuntimeException("读取公钥异常",e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(String.format("生成密钥工厂时没有[%s]此类算法", KEY_ALGORITHM),e);
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException("生成公钥对象异常",e);
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public static PrivateKey getRSAPrivateKeyByRelativePathFileSuffix(String relativePath, String fileSuffix)
	{
		return getRSAPrivateKeyByRelativePathFileSuffix( relativePath,  fileSuffix,  null);
	}
	
	
	public static PrivateKey getRSAPrivateKeyByRelativePathFileSuffix(String relativePath, String fileSuffix, String password)
	{
		return getRSAPrivateKeyByRelativePathFileSuffix( relativePath,  fileSuffix,  password,  KEY_ALGORITHM);
	}
	
	public static PrivateKey getRSAPrivateKeyByRelativePathFileSuffix(String relativePath, String fileSuffix, String password, String KEY_ALGORITHM){
		Resource r = new ClassPathResource(relativePath);
		File f;
		String filePath = "";
		try {
			f = r.getFile();
			filePath = f.getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException("",e);
		}
		return  getRSAPrivateKeyByAbsoluteFileSuffix( filePath,  fileSuffix,  password,  KEY_ALGORITHM);
	}
	
	
	
	public static PrivateKey getRSAPrivateKeyByAbsoluteFileSuffix(String filePath, String fileSuffix){
		return getRSAPrivateKeyByAbsoluteFileSuffix( filePath,  fileSuffix,  null);
	}
	
	public static PrivateKey getRSAPrivateKeyByAbsoluteFileSuffix(String filePath, String fileSuffix, String password){
		return getRSAPrivateKeyByAbsoluteFileSuffix( filePath,  fileSuffix,  password,  KEY_ALGORITHM);
	}
	
	/**
	 * 获取RSA私钥对象
	 * 
	 * @param filePath
	 *            RSA私钥路径
	 * @param fileSuffix
	 *            RSA私钥名称，决定编码类型
	 * @param password
	 *            RSA私钥保护密钥
	 * @param KEY_ALGORITHM
	 *            密钥算法
	 * @return RSA私钥对象
	 * @throws RuntimeException
	 */
	public static PrivateKey getRSAPrivateKeyByAbsoluteFileSuffix(String filePath, String fileSuffix, String password, String KEY_ALGORITHM)
			 {
		String keyType = "";
		if ("keystore".equalsIgnoreCase(fileSuffix)) {
			keyType = "JKS";
		} else if ("pfx".equalsIgnoreCase(fileSuffix) || "p12".equalsIgnoreCase(fileSuffix)) {
			keyType = "PKCS12";
		} else if ("jck".equalsIgnoreCase(fileSuffix)) {
			keyType = "JCEKS";
		} else if ("pem".equalsIgnoreCase(fileSuffix) || "pkcs8".equalsIgnoreCase(fileSuffix)) {
			keyType = "PKCS8";
		} else if ("pkcs1".equalsIgnoreCase(fileSuffix)) {
			keyType = "PKCS1";
		} else if ("yljf".equalsIgnoreCase(fileSuffix)) {
			keyType = "yljf";
		} else if ("ldys".equalsIgnoreCase(fileSuffix)) {
			keyType = "ldys";
		} else{
			keyType = "JKS";
		}
		BufferedReader br = null;
		InputStream in = null;
		try {
			in = new FileInputStream(filePath);
			PrivateKey priKey = null;
			if ("JKS".equals(keyType) || "PKCS12".equals(keyType) || "JCEKS".equals(keyType)) {
				KeyStore ks = KeyStore.getInstance(keyType);
				if (password != null) {
					char[] cPasswd = password.toCharArray();
					ks.load(in, cPasswd);
					Enumeration<String> aliasenum = ks.aliases();
					String keyAlias = null;
					while (aliasenum.hasMoreElements()) {
						keyAlias = (String) aliasenum.nextElement();
						priKey = (PrivateKey) ks.getKey(keyAlias, cPasswd);
						if (priKey != null) {
							break;
						}
					}
				}
			}else if("yljf".equals(keyType)){
				 br = new BufferedReader(new InputStreamReader(in));
				String s = br.readLine();
				PKCS8EncodedKeySpec priPKCS8=new PKCS8EncodedKeySpec(hexStr2Bytes(s));
				KeyFactory keyf=KeyFactory.getInstance("RSA");
				PrivateKey myprikey=keyf.generatePrivate(priPKCS8);
				return myprikey;
			}else if("ldys".equals(keyType)){
				byte[] b = new byte[20480];
				in.read(b);
				PKCS8EncodedKeySpec priPKCS8=new PKCS8EncodedKeySpec(b);
				KeyFactory keyf=KeyFactory.getInstance("RSA");
				PrivateKey myprikey=keyf.generatePrivate(priPKCS8);
				return myprikey;
			}else {
				 br = new BufferedReader(new InputStreamReader(in));
				StringBuilder sb = new StringBuilder();
				String readLine = null;
				while ((readLine = br.readLine()) != null) {
					if (readLine.charAt(0) == '-') {
						continue;
					} else {
						sb.append(readLine);
						sb.append('\r');
					}
				}
				if ("PKCS8".equals(keyType)) {
					PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decodeBase64(sb.toString()));
					KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
					priKey = keyFactory.generatePrivate(priPKCS8);
				} else if ("PKCS1".equals(keyType)) {
//					RSAPrivateKeyStructure asn1PrivKey = new RSAPrivateKeyStructure((ASN1Sequence) ASN1Sequence.fromByteArray(sb.toString().getBytes()));
					RSAPrivateKeyStructure asn1PrivKey = new RSAPrivateKeyStructure((ASN1Sequence) ASN1Sequence.fromByteArray(Base64.decodeBase64(sb.toString())));
					KeySpec rsaPrivKeySpec = new RSAPrivateKeySpec(asn1PrivKey.getModulus(), asn1PrivKey.getPrivateExponent());
					KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
					priKey = keyFactory.generatePrivate(rsaPrivKeySpec);
				}
			}

			return priKey;
		} catch (FileNotFoundException e) {
			throw new RuntimeException("私钥路径文件不存在",e);
		} catch (KeyStoreException e) {
			throw new RuntimeException("获取KeyStore对象异常",e);
		} catch (IOException e) {
			throw new RuntimeException("读取私钥异常",e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("生成私钥对象异常",e);
		} catch (CertificateException e) {
			throw new RuntimeException("加载私钥密码异常",e);
		} catch (UnrecoverableKeyException e) {
			throw new RuntimeException("生成私钥对象异常",e);
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException("生成私钥对象异常",e);
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	
	
	/*public static  byte[] hexStr2Bytes(String hexStr) {
		return new BigInteger(hexStr, 16).toByteArray();
	}*/
	public static final byte[] hexStr2Bytes(String s) {
		byte[] bytes; 
		bytes = new byte[s.length() / 2];
		for (int i = 0; i < bytes.length; i++) { 
			bytes[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2), 16);
		} 
		return bytes;
	}
	/**
	 * 字符数组16进制字符
	 * 
	 * @param bytes
	 * @return
	 */
	public static String bytes2Hex(byte[] bytes, int radix) {
		int size = 2;
		if ( 2 == radix) {
			size = 8;
		}
		StringBuilder sb = new StringBuilder(bytes.length * size);
		for (int i = 0; i < bytes.length; i++) {
			int integer = bytes[i];
			while (integer < 0) {
				integer = integer + 256;
			}
			String str = Integer.toString(integer, radix);
			sb.append(StringUtils.leftPad(str.toUpperCase(), size, "0"));
		}
		return sb.toString();
	}
	

}
