
package com.hulk.common.support.security.coder;


import com.hulk.common.support.security.Hex;
import com.hulk.common.support.security.SecurityCoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD加密组件
 * 
 * @author hulk
 * @version 1.0
 * @since 1.0
 */
public abstract class MDCoder extends SecurityCoder {

	/**
	 * MD2加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static byte[] encodeMD2(byte[] data)  {
		// 初始化MessageDigest
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD2");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// 执行消息摘要
		return md.digest(data);
	}

	/**
	 * MD4加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static byte[] encodeMD4(byte[] data)  {
		// 初始化MessageDigest
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD4");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// 执行消息摘要
		return md.digest(data);
	}

	/**
	 * MD5加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static byte[] encodeMD5(byte[] data)  {
		// 初始化MessageDigest
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// 执行消息摘要
		return md.digest(data);
	}

	/**
	 * Tiger加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static byte[] encodeTiger(byte[] data)  {
		// 初始化MessageDigest
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("Tiger");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// 执行消息摘要
		return md.digest(data);
	}

	/**
	 * TigerHex加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static String encodeTigerHex(byte[] data)  {
		// 执行消息摘要
		byte[] b = encodeTiger(data);
		// 做十六进制编码处理
		try {
			return new String(Hex.encode(b));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Whirlpool加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static byte[] encodeWhirlpool(byte[] data)  {
		// 初始化MessageDigest
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("Whirlpool");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// 执行消息摘要
		return md.digest(data);
	}

	/**
	 * WhirlpoolHex加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static String encodeWhirlpoolHex(byte[] data)  {
		// 执行消息摘要
		byte[] b = encodeWhirlpool(data);
		// 做十六进制编码处理
		try {
			return new String(Hex.encode(b));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * GOST3411加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static byte[] encodeGOST3411(byte[] data)  {
		// 初始化MessageDigest
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("GOST3411");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// 执行消息摘要
		return md.digest(data);
	}

	/**
	 * GOST3411Hex加密
	 * 
	 * @param data 待加密数据
	 * @return byte[] 消息摘要
	 * @throws Exception
	 */
	public static String encodeGOST3411Hex(byte[] data)  {
		// 执行消息摘要
		byte[] b = encodeGOST3411(data);
		// 做十六进制编码处理
		try {
			return new String(Hex.encode(b));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
