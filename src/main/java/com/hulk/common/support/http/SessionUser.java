package com.hulk.common.support.http;

import java.io.Serializable;

/**
 * 用户会话信息
 * @author hulk
 * @since 2018年7月22日 上午9:34:50
 */
@SuppressWarnings("serial")
public class SessionUser implements Serializable {
    private Long id;
    private String userName;
    private String userPhone;

    public SessionUser(Long id, String userName, String userPhone) {
        this.id = id;
        this.userName = userName;
        this.userPhone = userPhone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
