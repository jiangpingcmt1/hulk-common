package com.hulk.common.support.jedis;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import com.google.common.base.Splitter;
import com.google.common.collect.Sets;

/**
 * @author cmt
 *
 */
public class JedisClusterFactoryBean implements FactoryBean<JedisCluster>,
		InitializingBean ,DisposableBean {

	// private Resource addressConfig;
	private String addressCluster;

	private JedisCluster jedisCluster;
	private Integer timeout;
	private Integer maxRedirections;
	private GenericObjectPoolConfig genericObjectPoolConfig; //配置redis.clients.jedis.JedisPoolConfig

	private Pattern p = Pattern.compile("^.+[:]\\d{1,5}\\s*$");

	@Override
	public JedisCluster getObject()  {
		return jedisCluster;
	}

	@Override
	public Class<? extends JedisCluster> getObjectType() {
		return (this.jedisCluster != null ? this.jedisCluster.getClass()
				: JedisCluster.class);
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	private Set<HostAndPort> parseHostAndPort()  {
		try {
			Iterable<String> parts = Splitter.on(',').trimResults()
					.omitEmptyStrings().split(addressCluster);
			final Set<String> clusterHosts = Sets.newHashSet(parts);
			Set<HostAndPort> haps = new HashSet<HostAndPort>();
			for (String host : clusterHosts) {
				String val = host;
				boolean isIpPort = p.matcher(val).matches();
				if (!isIpPort) {
					throw new IllegalArgumentException("Illegal IP or port");
				}
				String[] ipAndPort = val.split(":");

				HostAndPort hap = new HostAndPort(ipAndPort[0],
						Integer.parseInt(ipAndPort[1]));
				haps.add(hap);
			}
			return haps;
		} catch (IllegalArgumentException ex) {
			throw  ex;
		} catch (Exception ex) {
			throw new RuntimeException("Failed to parse jedis configuration file", ex);
		}
	}

	@Override
	public void afterPropertiesSet()  {
		Set<HostAndPort> haps = this.parseHostAndPort();
		jedisCluster = new JedisCluster(haps, timeout, maxRedirections,
				genericObjectPoolConfig);

	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void setMaxRedirections(int maxRedirections) {
		this.maxRedirections = maxRedirections;
	}

	public void setAddressCluster(String addressCluster) {
		this.addressCluster = addressCluster;
	}

	public void setGenericObjectPoolConfig(
			GenericObjectPoolConfig genericObjectPoolConfig) {
		this.genericObjectPoolConfig = genericObjectPoolConfig;
	}

	@Override
	public void destroy() throws Exception {
		if (jedisCluster != null) {
			jedisCluster.close();
		}

	}
	
    /*    
    <bean name="JedisPoolConfig" class="redis.clients.jedis.JedisPoolConfig" >  
    <property name="maxWaitMillis" value="-1" />  
    <property name="maxTotal" value="1000" />  
    <property name="minIdle" value="8" />  
    <property name="maxIdle" value="100" />  
	</bean>  
	
	<bean id="jedisCluster" class="com.huateng.p3.common.jedis.JedisClusterFactory">  
	
	<property name="addressCluster" value="${addressCluster}" />   <!--  属性文件里  key的前缀 -->  
	  
	<property name="timeout" value="30000" />  
	<property name="maxRedirections" value="6" />  
	<property name="genericObjectPoolConfig" ref="JedisPoolConfig" />  
	</bean>  
	*/
	

}