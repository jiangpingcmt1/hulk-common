package com.hulk.common.component;

import com.google.common.base.MoreObjects;

import java.io.Serializable;


/**
 * 系统接口数据封装
 * @author cmt
 *
 * @param <T>
 */
public class R<T> implements Serializable {
	private static final long serialVersionUID = -750644833749014618L;
	private boolean success;
	private T result;
	private String errCode;
	private String errMsg;
	public boolean isSuccess() {
		return this.success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public T getResult() {
		return this.result;
	}

	public void setResult(T result) {
		this.success = true;
		this.result = result;
	}

	public String getErrCode() {
		return this.errCode;
	}

	public void setErrCode(String errCode) {
		this.success = false;
		this.errCode = errCode;
	}

	
	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("success", this.success).add("result", this.result)
				.add("error", this.errCode).add("errMsg", this.errMsg).omitNullValues().toString();
	}

	public static <T> R<T> ok(T data) {
		R<T> resp = new R<T>();
		resp.setResult(data);
		return resp;
	}

	public static <T> R<T> ok() {
		return ok(null);
	}
	
	public static <T> R<T> fail(String errCode ) {
		R<T> resp = new R<T>();
		resp.setErrCode(errCode);
		return resp;
	}

	public static <T> R<T> fail(String errCode ,  String errMsg) {
		R<T> resp = new R<T>();
		resp.setErrCode(errCode);
		resp.setErrMsg(errMsg);
		return resp;
	}
	
	
	 @Override
	    public boolean equals(Object o) {
	        if (this == o) {return true;}
	        if (o == null || getClass() != o.getClass()) {return false;}
			// if (!(o instanceof Response<?>))  return false;  
  
 
	        R<?> response = (R<?>) o;

	        if (success != response.success) {return false;}
	        if (!errCode.equals(response.errCode)) {return false;}
	        if (!result.equals(response.result)) {return false;}

	        return true;
	    }

	    @Override
	    public int hashCode() {
	        int result1 = (success ? 1 : 0);
	        result1 = 31 * result1 + result.hashCode();
	        result1 = 31 * result1 + errCode.hashCode();
	        return result1;
	   //     return Objects.hash(success,result, errCode);  
	  //      retrun  return new HashCodeBuilder(17, 37).append(success).append(result).append(errCode).toHashCode();  
	    }
	
}