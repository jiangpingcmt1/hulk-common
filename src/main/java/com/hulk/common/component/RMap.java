package com.hulk.common.component;

import java.util.HashMap;
import java.util.Map;

/**
 * 前台返回数据
 * 
 */
public class RMap extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public RMap() {
		put("code", 0);
	}
	
	public static RMap error() {
		return error(500, "未知异常，请联系管理员");
	}
	
	public static RMap error(String msg) {
		return error(500, msg);
	}
	
	public static RMap error(int code, String msg) {
		RMap r = new RMap();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	public static RMap ok(String msg) {
		RMap r = new RMap();
		r.put("msg", msg);
		return r;
	}
	
	public static RMap ok(Map<String, Object> map) {
		RMap r = new RMap();
		r.putAll(map);
		return r;
	}
	
	public static RMap ok() {
		return new RMap();
	}
	@Override
	public RMap put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
