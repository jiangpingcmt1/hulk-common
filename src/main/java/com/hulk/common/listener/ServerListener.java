package com.hulk.common.listener;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author hulk
 * @since 2018年4月21日 下午12:49:47
 */
@Deprecated
@Slf4j
public class ServerListener implements ServletContextListener {
    
    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
       log.info("=================================");
       log.info("系统[{}]启动完成!!!", contextEvent.getServletContext().getServletContextName());
       log.info("=================================");
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
    }
}
