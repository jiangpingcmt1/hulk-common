package com.hulk.common.listener;

import com.hulk.common.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 会话监听器
 * 
 * @author hulk
 * @version $Id: SessionListener.java, v 0.1 2014年3月28日 上午9:06:12 hulk Exp
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Slf4j
public class SessionListener implements org.apache.shiro.session.SessionListener {
    

    @Autowired
    RedisTemplate redisTemplate;

    /* (non-Javadoc)
     * @see org.apache.shiro.session.SessionListener#onStart(org.apache.shiro.session.Session) */
    @Override
    public void onStart(Session session) {
        session.setAttribute(Constants.WEBTHEME, "default");
        log.info("创建了一个Session连接:[" + session.getId() + "]");
        redisTemplate.opsForSet().add(Constants.ALLUSER_NUMBER, session.getId());
    }

    /* (non-Javadoc)
     * @see org.apache.shiro.session.SessionListener#onStop(org.apache.shiro.session.Session) */
    @Override
    public void onStop(Session session) {
        if (getAllUserNumber() > 0) {
            log.info("销毁了一个Session连接:[" + session.getId() + "]");
        }
        session.removeAttribute(Constants.CURRENT_USER);
        redisTemplate.opsForSet().remove(Constants.ALLUSER_NUMBER, session.getId());
    }

    /* (non-Javadoc)
     * @see org.apache.shiro.session.SessionListener#onExpiration(org.apache.shiro.session.Session) */
    @Override
    public void onExpiration(Session session) {
        onStop(session);
    }

    /** 获取在线用户数量 */
    public Integer getAllUserNumber() {
        return redisTemplate.opsForSet().size(Constants.ALLUSER_NUMBER).intValue();
    }
}
